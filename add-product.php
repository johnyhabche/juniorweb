<!DOCTYPE html>
<?php
include './includes/config.php';
?>
<html lang="en">

<head>
  <title>juniortest.johny.habche.com</title>

  <!-- Google Fonts -->

  <!-- Vendor CSS Files -->
  <link href="assests/bootstrap/bootstrap.min.css" rel="stylesheet">
  <link href="assests/css/style.css" rel="stylesheet">
  <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
    <script>

  $(function(){
           $("#DVD").css("display", "none");
           $("#Furniture").css("display", "none");
           $("#Book").css("display", "none");
    $("#productType").change(function() {
        var sel = $("select").val();
        if (sel==''){
           $("#DVD").css("display", "none");
           $("#Furniture").css("display", "none");
           $("#Book").css("display", "none");
        }else if (sel=='1') {
           $("#DVD").css("display", "block");
           $("#Furniture").css("display", "none");
           $("#Book").css("display", "none");
        }else if (sel=='2') {
           $("#Furniture").css("display", "block");
           $("#DVD").css("display", "none");
           $("#Book").css("display", "none");
        }else {
           $("#Book").css("display", "block");
           $("#DVD").css("display", "none");
           $("#Furniture").css("display", "none");           
        }
    });     
  })
        
</script>
</head>

<body>
  <!-- ======= Header ======= -->
  <header class="header" class="fixed-top ">
      <h1>ADD PRODUCT</h1>
      <div class="btn-container">
          <button name="submit_form" id="save" type="submit" form="product_form" class="btn btn-primary">Save</button>
        <a href="index.php"><button type="button" id="delete-product-btn" class="btn btn-danger">Cancel</button></a>
      </div>
      <br>
  </header><!-- End Header -->
  <div class="v-line"></div>
    <form action="add.php" method="post" class="product_form" name="form" id="product_form">
        <label class="label-form">SKU : </label>
        <input type="text" id="sku" name="sku"><br>
        <label class="error-message" id="sku-error"></label>
        <br />
        <label class="labelname-form">Name : </label>
        <input type="text" id="name" name="name"><br>
        <label class="error-message" id="name-error"></label>
        <br />
        <label class="label-form">Price : </label>
        <input type="number" id="price" name="price"><br>
        <label class="error-message" id="price-error"></label>
        <br />
        <label class="label-form">Type Switcher </label>
                <?php 
		$query = "SELECT * FROM type"; 
		$execute = $conn->query($query); 
            ?>
        <select name="type" id="productType">
        <option value=""></option>
        <?php
	   while($row = $execute->fetch_assoc()){
            ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['type_name']; ?></option>
                		  <?php 
	                       }
	       ?>
        <br />
        </select>
        <label class="error-message" id="switcher-error"></label>
    <div id="DVD">
        <label class="labelname-form"> Size (MB): </label>
        <input type="number" id="size" name="size">
        <label class="error-message" id="info"></label>
        <p>please provide DVD size! </p>
    </div>
    <div id="Furniture">
        <label class="labelname-form"> Height (CM): </label>
        <input type="number" id="height" name="height">
        
        <label class="labelname-form"> Width (CM): </label>
        <input class="md-input" type="text" id="width" name="width">
        
        <label class="labelname-form"> Lenght (CM): </label>
        <input class="last-input" type="number" id="lenght" name="lenght">  
        <label class="error-message" id="info"></label>
        <p>please provide dimensions in HxWxL format! </p>
    </div>
    <div id="Book">
        <label class="labelname-form"> Weight (KG): </label>
        <input type="number" id="weight" name="weight">
        <p>please provide the Weight of the product! </p>   
    </div>
        </form>

  <!-- Template Main JS File -->
  <script src="js/product-validation.js"></script>

</body>

</html>