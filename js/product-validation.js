var sku = document.getElementById("sku");
var nname = document.getElementById("name");
var price = document.getElementById("price");
var switcher = document.getElementById("productType");
var saveSubmit = document.getElementById("save");

var size = document.getElementById("size");
var height = document.getElementById("height");
var width = document.getElementById("width");
var lenght = document.getElementById("lenght");
var weight = document.getElementById("weight");

var skuErrorElement = document.getElementById("sku-error");
var nameErrorElement = document.getElementById("name-error");
var priceErrorElement = document.getElementById("price-error");
var switcherErrorElement = document.getElementById("switcher-error");
var infoErrorElement = document.getElementById("info");

var isSku = false;
var isName = false;
var isPrice = false;
var isSwitcher = false;
var isSize = false;

$(document).ready(function() {
    $('form').submit(function(e) {
        
        ////////////////////////////////////////////////
        
    var skuMessages = [];
    skuErrorElement.innerText = "";
    if(sku.value == null || sku.value === ""){
        skuMessages.push("sku is required");

    }
        else if(!isNaN(sku.value)){
          skuMessages.push("sku can't be a number");

    }

      if(skuMessages.length > 0){
        e.preventDefault();
        skuErrorElement.innerText = skuMessages.join(", ");
          isSku = false;
      }
    else isSku = true;
        
        ////////////////////////////////////////////////
        
    var nameMessages = [];
    nameErrorElement.innerText = "";
    if(nname.value == null || nname.value === ""){
        nameMessages.push("name is required");

    }

        else if(!isNaN(nname.value)){
          nameMessages.push("name can't be a number");

    }
    
      if(nameMessages.length > 0){
        e.preventDefault();
        nameErrorElement.innerText = nameMessages.join(", ");
          isName = false;
    }else isName = true;
        
        /////////////////////////////////////////////////
        
    var priceMessages = [];
    priceErrorElement.innerText = "";
    if(price.value == null || price.value === ""){
        priceMessages.push("price is required");

    }
        else if(price.value < 1){
            priceMessages.push("price can't be negative");
        }
    
      if(priceMessages.length > 0){
        e.preventDefault();
        priceErrorElement.innerText = priceMessages.join(", ");
          isPrice = false;
    }else isPrice = true;
        
        ////////////////////////////////////////////////
        
    var switcherMessages = [];
    switcherErrorElement.innerText = "";
    if(switcher.value == null || switcher.value === ""){
        switcherMessages.push("type is required");

    }
      
      if(switcherMessages.length > 0){
        e.preventDefault();
        switcherErrorElement.innerText = switcherMessages.join(", ");
          isSwitcher = false;
    }else isSwitcher = true;
        if(switcher.value == "1" && size.value == 0){
                e.preventDefault();
            switcherErrorElement.innerText = "please provide a Disk Size! ";
            
        }
        if(switcher.value == "2" && height.value == 0 && width.value == 0 && lenght.value == 0){
            e.preventDefault();
            switcherErrorElement.innerText = "please provide Furniture Dimensions! ";
            
        }
//        else if(height.value == 0){e.preventDefault();switcherErrorElement.innerText = "please provide all Dimensions fields! ";}
//        else if(width.value == 0){e.preventDefault();switcherErrorElement.innerText = "please provide all Dimensions fields! ";}
//        else if(lenght.value == 0){e.preventDefault();switcherErrorElement.innerText = "please provide all Dimensions fields! ";}
        if(switcher.value == "3" && weight.value == 0){
            e.preventDefault();
            switcherErrorElement.innerText = "please provide a book weight! ";
            
        }

        
    });
});